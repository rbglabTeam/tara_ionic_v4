import { Component, OnInit } from "@angular/core";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { Router } from "@angular/router";
import { UrlService } from "src/providers/url-service";
import { Network } from "@ionic-native/network/ngx";
import { NavController, MenuController } from "@ionic/angular";
import { ServicesAlertsProviderService } from "src/providers/services-alerts-provider.service";
import Util from "../../constants/content";
import { Storage } from "@ionic/storage";

@Component({
  selector: "app-login",
  templateUrl: "./login.page.html",
  styleUrls: ["./login.page.scss"]
})
export class LoginPage implements OnInit {
  public loginForm: FormGroup;

  constructor(
    private route: Router,
    private formBuilder: FormBuilder,
    private urlService: UrlService,
    private network: Network,
    private alertService: ServicesAlertsProviderService,
    private menuCtrl: MenuController,
    private navCtrl: NavController,
    private storage: Storage
  ) {
    this.menuCtrl.enable(false);
  }

  ngOnInit() {
    //this.getLocal();
    this.loginForm = this.formBuilder.group({
      email: ["", [Validators.required, Validators.email]],
      password: ["", [Validators.required, Validators.minLength(6)]]
    });
  }
  /**
   * Login Method
   */
  
  async onSignin() {
    debugger;
    try {
      if (this.loginForm.invalid) {
        return;
      }
      const object = {
        mail: this.loginForm.controls.email.value,
        password: this.loginForm.controls.password.value
      };
      if (this.network.type != 'none') {
        debugger;
        this.urlService.onServerCheck().then((status: any) => {
          if (status === true) {
            this.urlService.onLogin(object).then((data: boolean) => {
              if (data === true) {
               // this.route.navigate(['/dashboard']);
               this.navCtrl.navigateRoot(['/dashboard']);
              }else {

              }
            });
            console.log(this.loginForm.controls.email.value);
          } else {
            this.alertService.showToast(Util.SERVER_NOT_AVAILABLE, 4070);
          }
        })
      } else {
        this.alertService.showToast(Util.OFFLINE_CONTENT, 4070)
       }

    } catch (error) { }
  }

  async getLocal() {
    await this.storage.get("PROFILE").then(val => {
      let local = JSON.parse(val);
      if (local && local.profiledetails && local.profiledetails.token) {
        this.route.navigateByUrl("/dashboard");
      } else {
        this.route.navigateByUrl("/login");
      }
    });
  }
}
