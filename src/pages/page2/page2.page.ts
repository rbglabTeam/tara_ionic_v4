import {
  Component,
  OnInit,
  ViewChild,
  Input,
  ContentChild
} from '@angular/core';
import { SchemaModels } from 'src/providers/schemaObject';
import { Router } from '@angular/router';
import { NavController, MenuController } from '@ionic/angular';
import { StorageService } from 'src/providers/storageServices';
import { UrlService } from 'src/providers/url-service';
import { Network } from '@ionic-native/network/ngx';
import { ServicesAlertsProviderService } from 'src/providers/services-alerts-provider.service';
import Util from '../../constants/content';
import { Storage } from '@ionic/storage';
@Component({
  selector: 'app-page2',
  templateUrl: './page2.page.html',
  styleUrls: ['./page2.page.scss']
})
export class Page2Page implements OnInit {
  @ViewChild(ContentChild) content: ContentChild;
  @Input('returnValue') value: any;
  @Input(Util.EVEN_EMITTER_IMAGE_FOR_IMAGE_PATH_EMIT_KEY) offlineImage: any;
  public textString = '';
  public networkStatus = '';
  constructor(
    public schemaObject: SchemaModels,
    public route: Router,
    private menuCtrl: MenuController,
    private storageService: StorageService,
    private urlService: UrlService,
    private network: Network,
    private storage: Storage,
    private alertService: ServicesAlertsProviderService
  ) {
    this.menuCtrl.enable(true);
  }

  ngOnInit() {
    this.urlService.onStorageProfileDetails();
  }
  ionViewWillEnter() {
    const statusValue = this.storageService.update;
    if (statusValue === true) {
      this.textString = 'Update';
    } else {
      this.textString = 'Submit';
    }
  }
  ionViewDidEnter(){
   if (this.network.type != 'none') { 
       this.networkStatus = Util.ONLINE_CONTENT;
   } else {
    this.networkStatus = Util.OFFLINE_CONTENT;
   }
  }
  /**
   * find the  submit or update mehtod
   */
  validateMethod() {
    debugger
    if (this.textString === 'Update') {
      this.storageService.onUpdate();
      this.route.navigate(['/dashboard']);
    } else {
      this.storageService.submitForm();
      this.route.navigate(['/dashboard']);
    }
  }
   /**
   * find the  submit or update mehtod
   */
  submit() {
    if (this.network.type != 'none') {
      this.urlService.onServerCheck().then((status: boolean) => {
        if (status === true) {
          this.storage.get(Util.USER_PROFILE_KEY).then(async profile => {
            const prof = await JSON.parse(profile);
            if (prof) {
              if (prof.profiledetails) {
                this.storageService.token = await prof.profiledetails.token;
                this.validateMethod();
              }
            }
          });
        } else {
          this.alertService.showToast(
            Util.SERVER_NOT_REACHABLE_MOVE_TO_OFFLINE_MODE,
            4000
          );
        }
      });
    } else {
      this.storageService.addToUploadQueue();
      this.route.navigate(['/dashboard']);
    }
  }
  left() {
    this.route.navigate(['/page1']);
  }
}
