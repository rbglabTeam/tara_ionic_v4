import { UrlService } from "src/providers/url-service";
import { Component, OnInit } from "@angular/core";
import { DatePicker } from "@ionic-native/date-picker/ngx";
import { Router } from "@angular/router";
import { Network } from "@ionic-native/network/ngx";
import { StorageService } from "src/providers/storageServices";
import { SchemaModels } from "src/providers/schemaObject";
import { ServicesAlertsProviderService } from "src/providers/services-alerts-provider.service";
import Util from "../../constants/content";
import { MenuController } from "@ionic/angular";
import { Subscription } from "rxjs";
import { Storage } from "@ionic/storage";
@Component({
  selector: "app-page3",
  templateUrl: "./page3.page.html",
  styleUrls: ["./page3.page.scss"]
})
export class Page3Page implements OnInit {
  pendingCount: any;
  internetStatus;
  value;
  today = new Date();
  Unsubscribe: Subscription;
  public previousStatus: any;
  public dateOfToday = new Date().toISOString().slice(0, 10);
  local: any;

  constructor(
    private datePicker: DatePicker,
    private route: Router,
    private network: Network,
    private urlService: UrlService,
    private schemaService: SchemaModels,
    private menuCtrl: MenuController,
    private storageService: StorageService,
    private storage: Storage,
    private alertService: ServicesAlertsProviderService
  ) {
    this.menuCtrl.enable(true);
  }
  ionViewWillEnter() {
    this.menuCtrl.enable(true);
    this.getCurrentNetworkStatus();
    this.urlService.onStorageProfileDetails();
  }
  async ngOnInit() {
    this.getLocal();
    this.pendingCount = await this.storageService.getListPendingCountWithoutObservable();
    this.Unsubscribe = await this.storageService.offlineStatusCountOb.subscribe(
      count => {
        this.pendingCount = count;
        console.log("homepage offlinecount", this.pendingCount);
      }
    );
  }

  ionViewDidEnter() {
    try {
      debugger;
      this.urlService.onchangeStatus();
      this.Unsubscribe = this.urlService.networkConnectDisConnect.subscribe(
        async data => {
          this.internetStatus = await data;
          this.alertService.showToast(this.internetStatus, 4070);
          // this.displayNetworkUpdate(this.previousStatus);
          if (this.internetStatus === "online") {
            if (this.pendingCount != 0) {
              this.storage.get(Util.USER_PROFILE_KEY).then(async profile => {
                const prof = await JSON.parse(profile);
                if (prof) {
                  if (prof.profiledetails) {
                    this.storageService.token = await prof.profiledetails.token;
                    this.storageService.getUploadQueue();
                  }
                }
              });
            }
          }
        }
      );
    } catch (error) {}
  }

  ionViewDidLeave() {
    this.Unsubscribe.unsubscribe();
  }
  addEntry() {
    try {
      this.schemaService.resetForm();
      this.storageService.selectedDate = new Date().toDateString();
      //   this.user._id = "";
      this.datePicker
        .show({
          date: new Date(),
          mode: "date",
          androidTheme: this.datePicker.ANDROID_THEMES.THEME_HOLO_DARK
        })
        .then(
          date => {
            if (date <= this.today) {
              this.storageService.selectedDate = date.toDateString();
              this.urlService.selectedDate = date.toDateString();
              this.schemaService.acciDent.page1.selectedDate = date.toDateString();
              this.storageService.update = false;
              this.route.navigate(["/page1"]);
            } else {
              this.alertService.showToast(
                Util.FUTURE_DATE_NOT_ALLOWED_MESSAGE,
                4070
              );
            }
          },
          err => console.log(Util.ERROR_GETTING_DATE_MESSAGE, err)
        );
    } catch (error) {}
  }
  viewEntry() {
    try {
      this.schemaService.resetForm();
      if (this.network.type != "none") {
        this.datePicker
          .show({
            date: new Date(),
            mode: "date",
            androidTheme: this.datePicker.ANDROID_THEMES.THEME_HOLO_DARK
          })
          .then(
            date => {
              if (date <= this.today) {
                this.storageService.selectedDate = date.toDateString();
                this.urlService.selectedDate = date.toDateString();

                this.urlService.onServerCheck().then(async serverStatus => {
                  if (serverStatus === true) {
                    const dateformat = new Date().toDateString();
                    this.storageService.selectedDate = dateformat;
                    this.route.navigate(["/view-list-entry"]);
                  } else {
                    this.alertService.showToast(
                      Util.SERVER_NOT_AVAILABLE,
                      4070
                    );
                  }
                });
              } else {
                this.alertService.showToast(
                  Util.FUTURE_DATE_NOT_ALLOWED_MESSAGE,
                  4070
                );
              }
            },
            err => console.log(Util.ERROR_GETTING_DATE_MESSAGE, err)
          );
      } else {
        const networkStatus = this.getCurrentNetworkStatus();
        this.alertService.showToast(networkStatus, 4070);
      }
    } catch (error) {}
  }

  //     onChangeObj() {
  //      this.urlService.onServerCheck().then(async serverStatus => {
  //      if(serverStatus) {
  //        this.urlService.selectedDate =  this.value;
  //        this.storageService.selectedDate =  this.value;
  //         // this.navCtrl.setRoot(ViewListEntryPage);
  //         this.route.navigate(['/view-list-entry']);

  // console.log('date',this.value);
  // } else {

  // //  let connectionState = ' Reachable Try Again Later!'
  // //       this.toast.create({
  // //         message: `Sever is Not ${connectionState}`,
  // //         // message: `You are now ${connectionState} via ${networkType}`,
  // //         duration: 3000
  // //       }).present();
  //      }
  // })
  //  }
  getCurrentNetworkStatus() {
    if (this.network.type === "none") {
      return (this.internetStatus = "offline");
    } else {
      return (this.internetStatus = "online");
    }
  }

  // get from local
  async getLocal() {
    await this.storage.get("PROFILE").then(val => {
      this.local = JSON.parse(val);
      console.log(this.local);
    });
  }
}
