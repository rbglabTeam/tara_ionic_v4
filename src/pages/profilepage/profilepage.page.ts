import { Component, OnInit } from '@angular/core';
import { NavController, MenuController, Platform } from '@ionic/angular';
import { Storage } from '../../../node_modules/@ionic/storage';
import { Router } from '@angular/router';


@Component({
  selector: 'app-profilepage',
  templateUrl: './profilepage.page.html',
  styleUrls: ['./profilepage.page.scss'],
})
export class ProfilepagePage implements OnInit {
  section: string = "two";
  somethings: any = new Array(20);
  email;
  Address;
  desgination;
  Mob;

  ProfilepagePage;
  local: any;
  

  constructor(
    private menuCtrl: MenuController,
     private navCtrl: NavController, 
     public storage: Storage, 
     public platform: Platform,
      private route: Router) { 
        this.menuCtrl.enable(true);
        this.getLocal()
      }

  ngOnInit() {
  }
  // async ionViewCanEnter() {

  //   this.platform.ready().then(async ready => {
  //     // await this.menu.toggle();
  //     this.storage.get('profile').then(async  profile => {
  //       const prof = await JSON.parse(profile)
  //       if (prof) {
  //         if (prof.profiledetails) {
  //           this.email = await prof.profiledetails.email;
  //           this.Mob = prof.profiledetails.mobile;
  //           this.desgination = prof.profiledetails.type
  //         }
  //       }
  //     });
  //   });
  //   return true;
  // }
  backMenu() {
    this.route.navigate(['/dashboard']);

  }

  async getLocal(){
    await this.storage.get('PROFILE').then((val) => {
        this.local=JSON.parse(val)
        console.log(this.local);
        
      
      });
    }

}
