import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Network } from '@ionic-native/network/ngx';
import Util from '../constants/content';
import { ServicesAlertsProviderService } from './services-alerts-provider.service';
import { Storage } from '@ionic/storage';
import { Subject } from 'rxjs';
import { SchemaModels } from './schemaObject';
@Injectable({
  providedIn: 'root'
})
export class UrlService {
  public currentServerStatus = false;

  public profile = {
    mail: ' ',
    laslogged_out_time: ' ',
    version: 0.0,
    token: ' ',
    mobile: ' ',
    code: ' ',
    name: ' ',
    id: ' '
  };
  public appVersion = 0.0;
  public _id: any;
  accidentArray = [];
  entries = [];
  checkAccidentStatus = false;
  public profileDetails = new Subject<any>();
 public networkConnectDisConnect = new Subject<any>();
public accidentStatus = new Subject<any>();


  maxFileSize = 16000000;
  selectedDate: any;
  constructor(
    private http: HttpClient,
    private network: Network,
    private alertService: ServicesAlertsProviderService,
    private storage: Storage,
    private schemaService: SchemaModels
  ) {

  }
  /**
   * Login Method
   * @param jsonObject
   */
  onLogin(jsonObject) {
    try {
      this.alertService.showLoading(Util.LOGIN_LOADING_MESSAGE);
      return new Promise((resolve, reject) => {
        this.http
          .post<{
            code: any;
            _id: any;
            lastloggedout: any;
            mail: any;
            version: any;
            token: any;
            phNumber: any;
            name: any;
            status: any;
          }>(Util.SANTHOSH_API + Util.API_LOGIN_URL_PARAMS, jsonObject)
          .subscribe(
          async (res ) => {
              this.alertService.dismissLoading();
              if(res.code === 200){
              this.profile = {
                mail: res.mail,
                code: res.code,
                id: res._id,
                laslogged_out_time: res.lastloggedout,
                version: parseFloat(res.version),
                token: res.token,
                mobile: res.phNumber,
                name: res.name
              };
              await this.storage.set(
                Util.USER_PROFILE_KEY,
                JSON.stringify({ profiledetails: this.profile })).then( ( resv: any) => {
                  console.log('profile storage response', resv);
                });
              await this.onUpdateCurrentProfileDetails(this.profile);
              await this.getVersionAndProfileDetails(this.profile);
              await   this.alertService.showToast(res.status, 500);
              await   resolve(true);
              } else {
                await   this.alertService.showToast(res.status, 500);
              }
            },
            error => {
              this.alertService.dismissLoading();
              reject(false);
            }
          );
      });
    } catch (error) {
      this.alertService.dismissLoading();
      console.log('logincatch', error);
    }
  }
  /**
   * User Profile Details
   */
  onProfileDetails = () => {
    return this.profile;
  }
  /**
   * Update Token Details to AuthGuard
   */
    onTokenDetails() {
      if(this.profile.token) {
        return true;
      } else {
        return false;
      }
     
    }
  /**
   * Using Observable Update Current Profile Details
   */
  onUpdateCurrentProfileDetails(profile) {
    this.profileDetails.next(profile);
    this.profileDetails.asObservable();
  }
  /**
   * Server Availablity Check
   */
  onServerCheck() {
    try {
      // this.alert.showLoading(Util.SERVER_LOADING_MESSAGE);
      return new Promise((resolve, reject) => {
        this.http
          .get(Util.SANTHOSH_API + Util.API_SERVER_REACH_URL_PARAMS)
          .subscribe(
            (res: boolean) => {
              // await this.alert.dismissLoading();
              this.currentServerStatus = res;
              resolve(true);
            },
            async error => {
              // await this.alert.dismissLoading();
              reject(false);
              await this.alertService.showToast(
                Util.SERVER_NOT_AVAILABLE,
                Util.TOAST_DURATION
              );
            }
          );
      });
    } catch (error) {
      this.alertService.dismissLoading();
      this.alertService.showToast(Util.SERVER_NOT_AVAILABLE, Util.TOAST_DURATION);
      console.log('logincatch', error);
    }
  }

  /**
   * Network Status Return Function
   */
  onNetworkStatus = () => {
    // tslint:disable-next-line: triple-equals
    if (this.network.type != 'none') {
      this.alertService.showToast(Util.ONLINE_CONTENT, Util.TOAST_DURATION);
      return true;
    } else {
      this.alertService.showToast(Util.OFFLINE_CONTENT, Util.TOAST_DURATION);
      return false;
    }
  }
  /**
   * Current Version Check
   */
  onCurrentVersionDetails() {
    debugger;
    try {
    return new Promise((resolve, reject) => {
    this.http
      .get<{ version: any }>(Util.SANTHOSH_API + Util.APP_VERSION_URL_PARAMS)
      .subscribe(
        (res: any) => {
          if (res.version) {
            const value = res.version;
            this.appVersion = parseFloat(value);
            resolve(parseFloat(value));
            console.log('appversion', res);
          }
        },
        err => {
          reject(err);
          console.log(err);
        }
      );
    })
    // .finally(() => {
    //   debugger
    //       this.onStorageProfileDetails();
    // })
  } catch (error) {
      
  }
  }
  /**
   * Get  Profile Details From  Storage Plugin
   */
  onStorageProfileDetails() {
    try {
 debugger;
    this.storage.get(Util.USER_PROFILE_KEY).then(async profile => {
      const prof = await JSON.parse(profile);
      if (prof) {
        if (prof.profiledetails) {
          this.profile.mail = prof.profiledetails.mail;
            this.profile.laslogged_out_time = prof.profiledetails.laslogged_out_time;
            this.profile.version = prof.profiledetails.version;
            this.profile.token = prof.profiledetails.token;
            this.profile.mobile = prof.profiledetails.phNumber;
            this.profile.name = prof.profiledetails.name;
        }
        this.onUpdateCurrentProfileDetails(this.profile);
      }
    });
  } catch (error) {

  }
  }
/**
 * Get All Accident Details By Using Date
 */
  getAccidents() {
    try {
    
      const packet = {
      duration_date_time: this.selectedDate,
      token: this.profile.token
    };
      this.http.get<{data: any , status: any, code: any}>(Util.SANTHOSH_API + Util.API_ACCIDENT_LIST_URL_PARAMS + '?duration_date_time=' + packet.duration_date_time + '&' + 'token=' + packet.token)
     .subscribe(
      res => {
        if (res.code === 200) {
          this.entries = res.data;
          this.alertService.showToast(res.status, 4070);
        } else {
          this.checkAccidentStatus = true;
          this.accidentStatus.next(this.checkAccidentStatus);
          this.accidentStatus.asObservable();
        }
        console.log(res);
      },
      err => {
        console.log(err);
      }
    );
  } catch (error) {

  }
  }
  /**
   * 
   * @param _id get Signle Accident Details 
   */
  getAccident(_id) {
    try {
    return new Promise(resolve => {
      const packet = {
        accident_id: _id,
        token: this.profile.token
      };
      this.http.get<{status: any , code: any, data: any, _id: any}>(Util.SANTHOSH_API + Util.API_ACCIDENT_URL_PARAMS + '?accident_id=' + packet.accident_id + '&' + 'token=' + packet.token)
        .subscribe(
          res => {
            if (res.code === 200) {
              console.log('iserservice', res.data);
              this.schemaService.acciDent = res.data['data'];
              this.accidentArray.push(this.schemaService.acciDent);
              this._id = res.data['_id'];
              resolve(true);
            }

            console.log(res);
          },
          err => {
            console.log(err);
          }
        );
    });
  } catch (error) {

  }
  }
/**
 *  Get the Accident Object
 */
  getNewAccidentObject() {
    return this.accidentArray;
  }
/**
 *  User Logout Function
 */
  logoutFunc() {
    try {
    this.storage.get(Util.USER_PROFILE_KEY).then(async prof => {
      const idvalue = await JSON.parse(prof);
      if (idvalue.profiledetails) {
        console.log('appcompId', idvalue.profiledetails.id);
      }

      const packet = {
        _id: idvalue.profiledetails.id,
        lastloggedout: Date.now()
      };
      this.http.post<{code: any , data: any, status: any }>(Util.SANTHOSH_API + Util.API_LOGOUT_URL_PARAMS, packet)
        .subscribe(
          async res => {
            if (res.code === 204) {
              const entries = await res.data;
              this.alertService.showToast(res.status, 4070);
              console.log(res);
              const result = await this.storage.remove(Util.USER_PROFILE_KEY);
              console.log(result);
            }
          },
          err => {
            console.log(err);
          }
        );
    });
  } catch (error) {

  }
  }
  /**
   * Version Update Check in PlayStore
   */
  VersionUpdateFunc() {
    try {
    const packet = {
      version: '1.0'
    };
    this.http
      .post<{code: any , status: any}>(Util.SANTHOSH_API + Util.API_VERSION_URL_PARAMS, packet)
      .subscribe(
        async res => {
          if (res.code === 204) {
            // const entries = await res['data']
            console.log(res);
          }
        },
        err => {
          console.log(err);
        }
      );
    } catch (error) {

    }
  }

  /**
   * Network Connect Disconnect Status Using Observable
   */
  onchangeStatus() {

    this.network.onConnect().subscribe(data => {

      this.networkConnectDisConnect.next(data.type);
      this.networkConnectDisConnect.asObservable();

    });
    this.network.onDisconnect().subscribe(data => {

      this.networkConnectDisConnect.next(data.type);
      this.networkConnectDisConnect.asObservable();

    });
  }
  /**
   * 
   * @param profile Store Update User Profile Details in RXJS in order to subscribe the value
   */
  getVersionAndProfileDetails(profile) {
    this.profileDetails.next(profile);
    this.profileDetails.asObservable();
  }
}
