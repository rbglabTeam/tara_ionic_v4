import { Injectable } from '@angular/core';
import Swal from 'sweetalert2';
import { ToastController, LoadingController } from '@ionic/angular';
import { Router } from '@angular/router';
@Injectable({
  providedIn: 'root'
})
export class ServicesAlertsProviderService {
  loading: any;

  constructor(
    public toastCtrl: ToastController,
    public loadingCtrl: LoadingController,
    public route: Router
  ) {}

  async showLoading(content) {
    if (content) {
      this.loading = await this.loadingCtrl.create({
        spinner: 'crescent',
        message: content
      });
    }
    return await this.loading.present();
  }
 async dismissLoading() {
  await  this.loadingCtrl.dismiss().then(() => {
    console.log('loading dismiss console log');
  });
  }

  async showToast(content, duration) {
    const toast = await this.toastCtrl.create({
      message: content,
      duration,
      position: 'bottom'
    });
    toast.present();
  }
  genericAlertMessage(message, messagetype) {
    Swal.fire({
      title: '<strong> TARA </strong>',
      html: '<strong> <b>' + message + '</b> </strong>',
      type: messagetype,
      showCancelButton: false
    });
  }
  playStoreAlertMessage() {

    Swal.fire({
      title: '<strong> TARA </strong>',
      type: 'info',
      html: '<strong> <b>Kindly update App in Google Play Store!</b> </strong>',
      showCancelButton: true,
      confirmButtonText: 'Yes, update!',
      cancelButtonText: 'No, keep it'
    }).then((result) => {
      if (result.value) {
        window.open('https://play.google.com/store/apps/details?id=com.iitm.tara', '_system');
      } else {
     //  result.dismiss === Swal.DismissReason.cancel
       Swal.DismissReason.cancel;
       
      }
    })
  }
}
