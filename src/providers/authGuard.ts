import { Injectable } from '@angular/core';
import { CanActivate } from '@angular/router/src/utils/preactivation';
import { ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable, BehaviorSubject, ReplaySubject } from 'rxjs';
import { UrlService } from './url-service';
import { Storage } from '@ionic/storage';
import Util from '.././constants/content';
@Injectable({
    providedIn: 'root'
  })
export class AuthGuard implements CanActivate {
    path: ActivatedRouteSnapshot[];
    route: ActivatedRouteSnapshot;
    private loggedIn = false;
    public authenticated: ReplaySubject<boolean> = new ReplaySubject(); // starting app default as unauthorised

    constructor(public urlService: UrlService,
       public router: Router, private storage: Storage) {
      this.storage.get(Util.USER_PROFILE_KEY).then(async profile => {
        const prof = await JSON.parse(profile);
        if (prof) {
          if (prof.profiledetails) {
            const token = await prof.profiledetails.token;
            this.loggedIn = await true;
            this.authenticated.next(true);
            console.log('authgurad', this.loggedIn);
            //return this.router.navigate(['/dashboard']);
          }
        }
      });
    }

    public canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
       
      this.storage.get(Util.USER_PROFILE_KEY).then(async profile => {
        const prof = await JSON.parse(profile);
        if (prof) {
          if (prof.profiledetails) {
            const token = await prof.profiledetails.token;
            this.loggedIn = await true;
            this.authenticated.next(true);
            console.log('authgurad', this.loggedIn);
            return this.router.navigate(['/dashboard']);
          }
        }
      });
      return this.loggedIn;
      // if (!this.loggedIn) {
        //   console.log('canactivate', this.loggedIn);
        //   return this.router.navigate(['/login']);
        // }

      }
      public getGuardAuthentication(): boolean {
        return this.loggedIn;
      }
}
