import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { LoginPage } from 'src/pages/login/login.page';
import { AuthGuard } from 'src/providers/authGuard';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'login',
    pathMatch: 'full'
  },
  
  { path: 'login', loadChildren: '../pages/login/login.module#LoginPageModule' },

  { path: 'page1', loadChildren: '../pages/page1/page1.module#Page1PageModule' },
  { path: 'page2', loadChildren: '../pages/page2/page2.module#Page2PageModule' },
 // { path: 'dashboard', loadChildren: '../pages/page3/page3.module#Page3PageModule' },
     { path: 'dashboard', canActivate:[AuthGuard], loadChildren: '../pages/page3/page3.module#Page3PageModule' },

  { path: 'page4', loadChildren: '../pages/page4/page4.module#Page4PageModule' },
  { path: 'view-list-entry', loadChildren: '../pages/view-list-entry/view-list-entry.module#ViewListEntryPageModule' },
  { path: 'profilepage', loadChildren: '../pages/profilepage/profilepage.module#ProfilepagePageModule' },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
