import { Component, Input, Output, EventEmitter, OnInit } from '@angular/core';
import { UrlService } from 'src/providers/url-service';
import {
  MediaCapture,
  MediaFile,
  CaptureError,
  CaptureVideoOptions
} from '@ionic-native/media-capture/ngx';
import { Network } from '@ionic-native/network/ngx';
import { FilePath } from '@ionic-native/file-path/ngx';
import {
  StreamingMedia,
  StreamingVideoOptions
} from '@ionic-native/streaming-media/ngx';
import {
  FileTransfer,
  FileUploadOptions,
  FileTransferObject
} from '@ionic-native/file-transfer/ngx';
import { File } from '@ionic-native/file/ngx';
import { ServicesAlertsProviderService } from 'src/providers/services-alerts-provider.service';
import Util from '../../constants/content';
import { StorageService } from 'src/providers/storageServices';
@Component({
  selector: 'video-c',
  templateUrl: './video-c.component.html',
  styleUrls: ['./video-c.component.scss']
})
export class VideoCComponent implements OnInit {
  @Input('labelName') labelName: string;
  @Input('bindValue') value: any;
  @Output('returnValue') onFocusO: EventEmitter<any> = new EventEmitter();
  data: any;
  loading = null;
  netStatus: boolean = false;

  constructor(
    private mediaCapture: MediaCapture,
    private network: Network,
    private file: File,
    private transfer: FileTransfer,
    private streamingMedia: StreamingMedia,
    private urlService: UrlService,
    private storageService: StorageService,
    private alertService: ServicesAlertsProviderService
  ) { }

  ngOnInit() {
    if (this.network.type === 'none') {
      this.netStatus = false;
    } else {
      this.netStatus = true;
    }
    // this.userService.networkConnectDisConnect.subscribe(data => {
    //   if (data === 'online')
    //    this.netStatus = true;
    // });
    // console.log('component', this.data);
  }
  onfocusOut() {
    this.onFocusO.emit(this.value);
  }
  playVideo() {
    debugger;
    let options: StreamingVideoOptions = {
      successCallback: () => {
        console.log('Video played');
      },
      errorCallback: e => {
        this.alertService.showToast('Error Streaming', 3000)
        console.log('Error streaming');
      },
      orientation: 'portrait',
      shouldAutoClose: true,
      controls: false
    };
    this.streamingMedia.playVideo(this.getVideo, options);
  }

  recordVideo() {
    try {
      this.loading = 'Processing please wait...';
      let options: CaptureVideoOptions = { limit: 1 };
      this.mediaCapture.captureVideo(options).then(
        (res: MediaFile[]) => {
          this.data = res;
          if (this.data[0].size > this.urlService.maxFileSize) {
            this.loading = null;
            this.alertService.showToast(
              'Max media size is ' +
              this.urlService.maxFileSize / 1000000 +
              'MB',
              3000
            );
          } else {
            let capturedFile = res[0];
            let fileName = capturedFile.name;
            let dir = capturedFile['localURL'].split('/');
            dir.pop();
            let fromDirectory = dir.join('/');
            var toDirectory = this.file.dataDirectory;

            this.file.copyFile(fromDirectory, fileName, toDirectory, fileName)
              .then(
                async res => {
                  console.log('filevideo', res.nativeURL);
                  this.loading = null;
                  this.value = res.nativeURL;
                  this.onFocusO.emit(this.value);
                  if (this.network.type === 'none') {
                    const value = { video: this.data[0], videopath: this.value };
                    this.storageService.onSaveOfflineVideo(value);
                  } else {
                    await this.uploadFile();
                  }
                },
                err => {
                  console.log('err: ', err);
                }
              );
          }
        },
        (err: CaptureError) => {
          this.loading = null;
          console.error(err);
        }
      );
    } catch (e) {
      console.error(e);
    }
  }
  get getVideo() {
    if (this.netStatus)
      return Util.SANTHOSH_API + 'media/video?video=' + this.value;
    else return this.value;
  }

  uploadFile() {
    try {
      this.alertService.showLoading(Util.VIDEOS_UPLOADING_MESSAGE)
      const fileTransfer: FileTransferObject = this.transfer.create();

      let options: FileUploadOptions = {
        fileKey: 'video',
        fileName: this.data[0].name,
        chunkedMode: false,
        mimeType: this.data[0].type,
        headers: {}
      };
      const filePath = this.data[0].fullPath;

      fileTransfer
        .upload(
          filePath,
          Util.SANTHOSH_API + 'media/video?video=' + this.value,
          options
        )
        .then(
          data => {
            this.alertService.dismissLoading()
            this.loading = null;
            this.alertService.showToast('Video uploaded', 2000);
          },
          err => {
            this.loading = null;
            console.log(err);
          }
        );
    } catch (e) {
      console.error(e);
    }
  }
}
