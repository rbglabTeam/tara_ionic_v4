
import { NgModule,CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA  } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BrowserModule } from '@angular/platform-browser';
import { IonicModule } from '@ionic/angular';
import { StringelementComponent } from './stringelement/stringelement.component';
import { NumberelementComponent } from './numberelement/numberelement.component';
import { MultiComponent } from './multi/multi.component';
import { LocationCComponent } from './location-c/location-c.component';
import { ImageCComponent } from './image-c/image-c.component';
import { AudioCComponent } from './audio-c/audio-c.component';
import { VideoCComponent } from './video-c/video-c.component';
import { DatetimeComponent } from './datetime/datetime.component';
import { BooleanCComponent } from './boolean-c/boolean-c.component';
import { RadioCComponent } from './radio-c/radio-c.component';
import { AnyoneCComponent } from './anyone-c/anyone-c.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@NgModule({
    declarations: [StringelementComponent,
        NumberelementComponent,
        MultiComponent,
        ImageCComponent,
        AudioCComponent,
        VideoCComponent,
        DatetimeComponent,
        BooleanCComponent,
        RadioCComponent,
       AnyoneCComponent,
    
  ],
    imports: [
        BrowserModule,
        CommonModule,
        FormsModule,
      ReactiveFormsModule,
        IonicModule.forRoot()
    ],
     exports: [StringelementComponent,
        NumberelementComponent,
        MultiComponent,
      
        ImageCComponent,
        AudioCComponent,
        VideoCComponent,
        DatetimeComponent,
        BooleanCComponent,
        RadioCComponent,
      AnyoneCComponent,
    ],
    entryComponents: [],
    schemas: [
        NO_ERRORS_SCHEMA, CUSTOM_ELEMENTS_SCHEMA 
    ],
})
export class ComponentsModule { }
