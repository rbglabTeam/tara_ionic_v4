import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NumberelementComponent } from './numberelement.component';

describe('NumberelementComponent', () => {
  let component: NumberelementComponent;
  let fixture: ComponentFixture<NumberelementComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NumberelementComponent ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NumberelementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
