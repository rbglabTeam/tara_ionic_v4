import { Component, OnInit, Input, Output, EventEmitter } from "@angular/core";
import { Geolocation } from '@ionic-native/geolocation/ngx';



@Component({
  selector: "location-c",
  templateUrl: "./location-c.component.html"
})
export class LocationCComponent implements OnInit {
  isWeb: boolean = false;
  @Input("labelName") labelName: string;
  @Input("bindValue") value: any;
  @Output("returnValue") onFocusO: EventEmitter<any> = new EventEmitter();
  constructor(public geolocation: Geolocation) { }

  ngOnInit() { }
  // async platformTest() {
  //   if (this.platform.is("desktop") || this.platform.is("mobileweb")) {
  //     //Do Cordova stuff
  //     this.isWeb = await true;
  //     console.log("web");
  //   } else {
  //     //Do stuff inside the regular browser
  //     console.log("mob");

  //     this.isWeb = await false;
  //   }
  // }
  async getGeoLocation() {

    this.geolocation
      .getCurrentPosition()
      .then(data => {
        this.value =
          "Lat:" +
          data.coords.latitude +
          "\n" +
          "Long:" +
          data.coords.longitude;

        //  this.value['Lat']= data.coords.latitude;
        //   this.value['Long'] = data.coords.longitude;
        this.onFocusO.emit(this.value);
      })
      .catch(error => {
        console.log("Error getting location", error);
      });
  }
}

