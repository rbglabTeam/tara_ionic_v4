import { Component, OnInit,Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-boolean-c',
  templateUrl: './boolean-c.component.html',
  styleUrls: ['./boolean-c.component.scss'],
})
export class BooleanCComponent implements OnInit {
  @Input('labelName') labelName: string;
  @Input('bindValue') value: any;
  @Output('returnValue') onFocusO: EventEmitter<any> = new EventEmitter();
  constructor() { }

  ngOnInit() {}
  onChangeObj() {
    this.onFocusO.emit(this.value)
  }
}
